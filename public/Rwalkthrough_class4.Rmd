---
title: "TCGA Analysis Notebook for BioCanRx / SCN course, Class 4"
author: "Gareth Palidwor"
date: "`r format(Sys.Date(),'%B %d, %Y')`"
output:
  html_document:
    toc: yes
  pdf_document:
    toc: yes
---

# Introduction
This notebook is a walk through of analyzing and visualizing The Cancer Genome Atlas data extending what we've learned in the previous classes.

First we'll install  package we'll need today, load all the libraries and run a script to provide us with TCGA and DESeq2 data that we had at the end of last class.

You can run these chunks while I give a brief presentation.

# Walkthrough

## New package install
```{r eval=FALSE, include=T}
#these are one-time installs that need only be run the first time you run the walk through.
#install.packages("pheatmap")         # Pretty Heatmaps
#install.packages("EnhancedVolcano")  # Volcano plots
#install.packages("RColorBrewer")     # color palette tools
#BiocManager::install("EnhancedVolcano")
#BiocManager::install("EDASeq")
```


## loading libraries
```{r}
library("dplyr")
library("DESeq2")
library("ggplot2")
library("ggrepel")
library("TCGAbiolinks")
library("pheatmap")         
library("EnhancedVolcano")  
library("RColorBrewer")
library("EDASeq")
```

## Regenerating TCGA data sets
This is a re-run of our analysis from the class last week to get us to the same state we were in then.

This should take just a few minutes to run.

```{r}
gdcinfo<-getGDCInfo()

query_luad_types<-GDCquery(project = "TCGA-LUAD",                 
                   data.category = "Transcriptome Profiling",     
                   data.type = "Gene Expression Quantification")   

norm_subset<-subset(getResults(query_luad_types),  
              sample_type=="Solid Tissue Normal" & 
              analysis_workflow_type=="STAR - Counts")

all_samples_from_pwn<-subset(getResults(query_luad_types),
                             analysis_workflow_type=="STAR - Counts" &
                             cases.submitter_id %in% norm_subset$cases.submitter_id)

query.luad.cts <- GDCquery(project = "TCGA-LUAD", #TCGA  Lung Adenocarcinoma 
                           data.category = "Transcriptome Profiling", #RNAseq
                           data.type = "Gene Expression Quantification", #RNAseq 
                           workflow.type = "STAR - Counts",#raw read map counts
                           sample.type=c("Primary Tumor","Solid Tissue Normal"),
                           barcode = all_samples_from_pwn$cases,
                           legacy = FALSE) #only get current harmonized (hg38) not legacy (hg19) data

GDCdownload(query.luad.cts) 

luad.cts <- GDCprepare(query.luad.cts)

dds <- DESeqDataSet(luad.cts, design = ~ sample_type)

dds <- DESeq(dds)
```

To avoid having to rerun everything in future we can save our R environment as a file; note that this will be very large, more than 200M. 


We can save the current workspace image (including libraries) with the `save.image` command. The tilde (~) is the path to you r home directory which varies by Operating System.
```{r}
save.image(file="~/TCGA_dds_env.RData")
```

We can clear the workspace ( Session | Clear Workspace)  and see how the Environment is cleared.

If we load the above saved data:
```{r}
load(file="~/TCGA_dds_env.RData")
```
The environment is restored by loading this environment.


This allows you to save your environment at a known state so you can revert to it later. It is particularly useful if you are doing exploratory analysis that takes a long time to set up.


## Further analysis of DESeq2 data

Note that differential expression analysis of STAR count data can also be done entirely within `TCGAbiolinks` using the `TCGAanalyze_DEA`, but we prefer DESeq2 as we are most familiar with it. `TCGAanalyze_DEA` uses `edgeR` rather than `DESeq2`, they are quite similar analysis techniques, both using a Negative Binomial Model

We summarize the results of thie previously generated DESeq2 contrast where we compared  `Solid.Tissue.Normal` vs `Primary.Tumor` samples.  

With this comparison:

* Genes that have generally higher  expression in Normal than Tumor samples will have a _positive_ fold change.
* Genes that have a generally higher expression in Tumor than Normal samples will have a _negative_ fold change. 


```{r}
res <- results(dds, name = "sample_type_Solid.Tissue.Normal_vs_Primary.Tumor")
summary(res)
#View(res)
```

The default parameters give rather a lot of results, calling a high percentage of all  genes as significantly different.

We'll re-run it but but instead of using default threshold parameters of accepting any log2 Fold change (default `lfcThreshold=0`) and a high adjusted p-value (default `alpha=0`), we'll use `alpha=0.05` and `lfcThreshold=1`.



```{r}
res <- results(dds, name = "sample_type_Solid.Tissue.Normal_vs_Primary.Tumor",
               lfcThreshold=1,alpha=0.05,)
summary(res)
```

This revised contrast returns fewer genes as significant as the criterion is far more stringent. As we have a large number of samples per condition, it's possible to get very significant adjusted p-values and low fold changes at the same time. We see this much less in experiments where there are just a few samples per condition.

Note that the filter parameters are added to the `DESeq` result object but don't actually remove any rows. They can be used for downstream analysis like the summary command above.

```{r}
head(res)
```
Looking at the table, we note that the gene identifiers are from ENSEMBL. These which uniquely identify a gene. The ENSEMBL identifier prefix identifies the species. `ENSG` in  `ENSG00000000003` prefix indicates that it is a human gene. Mouse genes have a `ENSMUSG` prefix, Rat genes have a `ENSRNOG` prefix.

See: https://useast.ensembl.org/info/genome/stable_ids/prefixes.html  for a table of species / and strain specific ensembl prefixes.

In some cases you may see these identifiers with a numeric suffix e.g. `ENSG00000000003.2`. The suffix indicates the annotation version for that gene and indicates an annotation change for that gene between annotation releases.  There are similar annotations for transcripts (human: `ENST` prefix). 

While these identifiers are unambiguous, they aren't particularly readable. We generally prefer gene symbols, if possible, for interpretability.

Luckily, as we saw previously, the originally loaded `luad.cts` object contains gene annotations in the `rowData`.

```{r}
rowData(luad.cts)
```
There are three columns, including `gene_name` which is a mix of HUGO Gene Nomenclature Committee (HGNC) names and other identifiers for names not covered by HGNC. 

Conveniently the values are in the same order as the DESeq2 results, so we can add that as additional annotations directly to the `DESeq2Results` object.

You can get many other gene level annotations from ENSEMBL using the ENSEMBL `biomaRt` library; we don't have time to get into that in this class but Chris has a detailed example in the 2022 SCN RNA Workshop DESeq2 script (see class links).

```{r}
res$gene_name<-rowData(luad.cts)$gene_name
head(res)
```

In the homework from last week, I asked that you sort the results of the fold change to get the three genes with the lwest, the table can be sorted various ways; for example you could export it to excel and sort it there.  The easiest way is to use the R base library `order` function which takes a vector (or column) of values and returns the order they should be in; the defaltis by increasing value so the lowest value is at the top.
```{r}
res<-res[order(res$padj),]
head(res)
```
## Visualizations

### Principal Component Analysis (PCA)

Below, we re-plot the PCA plot from last class with a little more explanation.

A PCA of gene expression data takes the normalized values of gene expression for the samples and creates a ranked list of uncorrelated variables known as _Principal Components_. This is used to visualize patterns in data sets and give insights into the properties of the data.

```{r}
rld<-vst(dds) #Variance STabilizing Transform 
plotPCA(rld,intgroup=c("sample_type","gender")) + 
            geom_text_repel(aes(label=colData(dds)$patient),hjust=0,vjust=1,size=2)
```

We see (again) that the cancer/normal samples are well divided by PC1 which constitues 40% of all variance.

### Hierarchic Clustering and Heatmaps 

Hierarchical clustering is calculated some form of distance (in this case Euclidian distance) between the same vst-transformed normalized count values we used for PCA. This is another useful diagnostic, particularly for smaller numbers of samples, where it can help spot outliers.

Euclidean distance is similar to distance in 2 dimensions, `sqrt(x^2 + y^2)`  but applied to `n` dimensions. Each sample is a position in a space of `n` dimensions where those dimensions correspond to genes and the values correspond to normalized `vst` transformed expression values. The matri of the heatmap corresponds to the pairwise distances between samples.

```{r}
## Set up a colour ramp palette (shades of blue)
colours<-colorRampPalette(rev(brewer.pal(9,'Blues')))(255)

## Calculate the Euclidian distance between samples, based on rlog-transformed read counts per gene
## rld is an object containing a lot of information - we're only interested in the assay() slot that contains the transformed counts
## the t() function transforms the matrix of rlog counts so that samples are in rows, and genes in columns
## dist() calculates the distance between the rows based on the values in the columns.

sampleDists<-dist(t(assay(rld)))

## Reshape the vector of distances into a matrix

sdm<-as.matrix(sampleDists)
rownames(sdm)<-colData(dds)$barcode
colnames(sdm)<-colData(dds)$barcode

colAnn<-data.frame(sampleType=colData(dds)$shortLetterCode)
rownames(colAnn)<-colData(dds)$barcode
annCols<-list(sampleType=c(NT="green",TP="red"))

## Plot heatmap of sample distances
pheatmap(sdm,color=colours,border_color=NA,legend=T,show_rownames=F,show_colnames=F,
annotation_col=colAnn,annotation_colors=annCols)

```

In the plot the X and Y axes dendrograms represent the sample similarity, with samples clustered by euclidian distance. Lower distances are darker, and higher distances are lighter, see the scale.

A sample type annotation column has been added and you can see that the Normal (green) and Cancer (red) samples cluster together.

### Volcano plot

Next, we'll look at a rather nicer "volcano plot" of our data. A volcano plot is a visualization of RNASeq data in the form of a scatterplot which shows _statistical significance vs fold change_.

We'll use the `EnhancedVolcano` plotting package we installed and loaded earlier to plot our results, labeling the results with the `gene_name` value we added above.

```{r}
 EnhancedVolcano(res,lab=res$gene_name,x="log2FoldChange",y="padj",
                    title="Normal vs Cancer",
                     pCutoff=0.05,FCcutoff=1,
                 legendPosition="right",subtitle="",axisLabSize=10,labSize=3)
```

Can you guess why it's called a volcano plot?

For this plot the Y axis represents statistical significance, with the adjusted-p value log base 10 transformed and negative. The log10 transform scales the significance to be more visualizable and interpretable. Making it negative means that more significant (lower) adjusted p-values will be higher on the plot.

The colours highlight significance status (p-value, fold change) as per the legend.

As we discussed previously:

* Genes on the right have _positive fold change_, these have generally higher  expression in Normal than Tumor
* Genes on the left have a _negative fold change_,these have generally higher expression in Tumor than Normal.

Once again this plot is both diagnostic and and can be informative.

## Enrichment Annotation Analysis 

We can take the list of genes that are higher in tumor vs normal (upregulated in cancer) with our previously chosen thresholds for `log2FoldChange` and `padj` and determine if these genes are more enriched for various annotations (GeneOntology and Pathways) than would be expected from a random selection of genes.

From the `TCGAvisualize_EAbarplot` docs, the visulaization shows:

> The most statistically significant canonical pathways identified in DEGs list are listed according to their p-value corrected FDR (-Log) (colored bars) and the ratio of list genes found in each pathway over the total number of genes in that pathway (Ratio, red line).

The results are output as a pdf file, not in the report.

```{r}
downfold_genes<-subset(res,log2FoldChange< -1 & padj<0.05)$gene_name
nrow(downfold_genes)
ansEA <- TCGAanalyze_EAcomplete(TFname="Normal vs Tumor -FC",downfold_genes)
length(downfold_genes)

TCGAvisualize_EAbarplot(tf = rownames(ansEA$ResBP), 
                        GOBPTab = ansEA$ResBP,
                        GOCCTab = ansEA$ResCC,
                        GOMFTab = ansEA$ResMF,
                        PathTab = ansEA$ResPat,
                        nRGTab = downfold_genes, 
                        filename="~/EA_output.pdf",
                        nBar = 10)

```
This is a fast and easy way to perform enrichment analysis within the `TCGAbiolinks` package there are various other tools that give more control. See the 2022 SCN RNASeq Workshop links one the course page for a detailed presentation and exercise of how to use `g:Profiler` and `DAVID` online annotation enrichment tools.

## Survival plot

It's also possible to generate a survival analysis from the clinical data. In general survival analyses consider time between entry into a study and some event. In this case we're looking at the "death" event, and comparing whether there are significant differences between mortality for different conditions.

In this case we're simply comparing gender curves, but we could for example compare treatments or whether a high gene expression is diagnostic of higher risk. This can be relevant to targeted treatments.

The results are output as a pdf file, not in the report.

```{r}
  #load the clinical data for LUAD 
  clin.luad <- GDCquery_clinic("TCGA-LUAD", "clinical")
  head(clin.luad)
  
  #plot the survival curves and difference analsysi
  TCGAanalyze_survival(clin.luad,
  "gender",
  main = "TCGA Set\n LUAD",height = 10, width=10,filename = "~/survival.pdf")
```
```


# Environment

Some details on the environment this was run in so the analysis can be replicated.

```{r}
version
sessionInfo()
```
