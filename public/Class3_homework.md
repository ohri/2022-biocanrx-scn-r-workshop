## Class 3 Homework (optional)
_September 21, 2022, BioCanRx/SCN R workshop_

This homework exercise is mainly re-running the Class 3 analysis using breast cancer data rather than lung cancer to get more direct experience analyzing TCGA data.

* What is the TCGA project identifier for Breast Cancer data (analogous to TCGA-LUAD)?
* What data types are available? Check using TCGAbiolinks and the TCGA web interface.
* For data.category `Transcriptome Profiling` data.type `Gene Expression Quantification` specifically workflow.type `STAR - Counts`, what are the different tissue.types and how many are there of each?
* Extract all the data of type `Star - Counts` for patients which have `Solid Tissue Normal`  data.
* Perform a fold change comparison betweenthe tissue types, and visualize the data in a PCA plot and Volcano plot
* What are the top 3 genes by adjusted p-value?  Go to the ensembl site and look up the gene symbols from the Ensembl identifiers.
