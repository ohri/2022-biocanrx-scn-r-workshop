## 2022 BioCanRx/SCN R Workshop, September 7

### R code from Class 1

_Ottawa Bioinformatics Core Facility_
_Gareth Palidwor and Christopher Porter_
This is the code portion of an R and R studio usage demonstration.

It's best for learning if you type the code in yourself manually and don't just cut and paste.

### Helpful links 

* [R Style guide by Hadley Wickham](https://style.tidyverse.org)
* [R language operators](https://www.statmethods.net/management/operators.html)
* [Assignment operators in R](https://www.r-bloggers.com/2010/11/assignment-operators-in-r)
* [An assortment of R cheat sheets](https://www.rstudio.com/resources/cheatsheets/)
* [CRAN R libraries](https://cran.r-project.org/)
* [Bioconductor bioinformatics libraries](https://www.bioconductor.org/)


## Code examples

### R operators
```{r}
# =, <- are both assignment operators, subtle differences
# we will use <- for assignment
# we will use = for passing arguments (we'll see this with lists)
# variables can be named with text, underscores and numbers (can't start iwth a number)
x<-1
y=1
x
y
cheese_5<-y

#see operators in the links above
#operators are special sybols that tell the R intepreter to perform some operation and return a results

#arithmetic
3+2

#Logical tests
3>2 #is 3 greater than 2
3==2  #is 3 equal to 2
3!=2  #is 3 not equal to 2

#check operators guide linked above
```

### R data types
```{r}
#class function returns class of a given object, we'll review the various classes.
class("c")

class(1)

class(FALSE)
```

### Numeric data types
```{r}
#numeric data types: integers and float values are numeric
#numeric are double precision floats, represented as 64 bits
5*9

9/5

pi

#There is a value for infinity and negative infinity

#inifinity
1/0 
#negative infinity
-1/0 

#zero divided by zero is a special value, not a number (NaN)
0/0 

#not available (NA) indicates a missing value
NA

```

### Logical data types
```{r}
#logical are TRUE and FALSE
# also T and F but those can be redefined
T
F

# can be a result of tests
3>2
is.na(NA)
3==3

#and can be assigned
logical_variable <- 3==3
```

### Vectors
A vector is a ordered set of data types, all of one class.
Different than a _list_ which we will cover next.

```{r}
# a vector of characters
mychars<-c("a","b","c","d","e")  

#a vector is identified as that class
class(mychars)

# a vector of numeric 
mynums<-c(1,2,3,4,5)  

# a vector of logical 
mybools<-c(T,F,T,F,T)  

#a factor is a categorical type
myfactor<-as.factor(c("alive","dead","alive","dead","alive")) 

# a vector can only be of one type or it will be coerced to one type
c(1,"a",FALSE) #note how these are all converted (coerced) to characters
#R is a weakly typed language and does type conversions commonly and silently
#if you're not careful.
#This is a major issue with R and a common source of errors.

#there are various ways to specify vectors
1:10  #return values from 1:10
rep("a",6) #return a *rep*eated list of 6 "a" 

#lets generate a vector containing numbers from 1 to 5
mynums <-1:5

#vectors can be accessed by index (first index is 1 not zero)

#return element 5 from the vector
mynums[5] 

#return elements 3 and 4 from the vector as another vector
mynums[3:4] 

#return the vector removing element 3
mynums[-3] 

#return elements 1:7 from the vector (of length 5). Note the NAs
#those values are NAs as the remaining elements are Not Available
mynums[1:7] 	 

#NA's can also be indicative of errors so keep an eye out for that.

#you can concatenate vectors by putting them together in order
#it just ads the list together it doesn't nest them in any way
c(mynums,mynums)

```

### Lists

Lists are a vector like type can contain elements of different types (even other lists, that can themselves contain lists and so on).
```{r}
#List elements can be named, are always indexed 

#lets create a list of two vectors that we created earlier
mylist<-list(nums=mynums,bools=mybools) 

#they're indexed in a manner similar to vectors

#return the first two elements of mylist
mylist[1:2] 

#return the first element of mylist
mylist[1] 

#return the contents of the first element of mylist
mylist[[1]]

#return the element of mylist names "nums"
mylist[["nums"]] 

#tab completion works with lists; this is helpful if it's a returned data type

#mylist$

```

### Factors
```{r}
#factors are a variable type that can have only a specific enumerated set of values.
#they're like categories.
myfactor<-as.factor(c("alive","dead","alive","dead","alive")) 

#levels of a factor are the categories in order
levels(myfactor)

#these can be coerced to numeric, and we get the category ordinal values as we saw above
as.numeric(myfactor)

#you can specify the order of the factors 
factor(c("alive","dead","alive"),levels=c("alive","dead"))
factor(c("alive","dead","alive"),levels=c("dead","alive"))


```

### Data frames

A very important data type in R. 

Imagine them like a spreadsheet. Like a matrix but it can contain multiple data types in the columns.

```{r}
#creating a data framed using the types we've created previously
mydf<-data.frame(mynums,myfactor,mychars,mybools) 

#summary of data frame columns for a quick overview
summary(mydf)

#we can also inspect it in the environment pane

#note that they support tab completion (column names)
#mydf$

#function that returns the number of rows
nrow(mydf)

#function that returns the number of columns
ncol(mydf)

#function that returns the colnames (also can assign colnames)
colnames(mydf)


#reading and writing data frames 
write.table(mydf,file="mydf.txt",row.names=F,col.names=T,sep="\t",quote=F)

#read it into a different variable from the file
mydf_read<-read.table(file="mydf.txt",header=T,sep="\t")

#find the current directory you wrote it to
getwd()
```

### Here's how you get code documentation
```{r}
?sqrt
help(sqrt)
??sqrt #to search all libraries
```


### Functions

A function is a way to put together a bunch of code to make it reusable within your program.

It takes arguments and returns a value.

If you find yourself cutting and pasting code, that’s good indication you should create a function to reuse your code. 

`sqrt` is a very simple function


```{r}
sqrt(9)

#sum is a little more complicated and shows different features. It takes a  vector as an argumentand returns a value that’s the sum of the vector.
# ma.rm=FALSE is an optional named argument with a default value 
# This is a deafult setting you can override, na.rm=T 
# When using a function always look at the arguments, in particular the default arguments (can be unexpected)
?sum

sum(mynums) 

sum(c(mynums,NA)) 

sum(mynums,na.rm=T) 

sum(c(1,2,3,4,6),na.rm=T) 

```

### Functions

Say we want to convert zero Farenheit to Celsius:
```{r}
f<-0
c<-(f-32)*5/9
c
```

How do we generalize that?

### A simple function to convert Farenheit to Celsius
````{r}
# a simple function to convert Fareheit to celsius
# we generate the function and assign it to a variable
ftoc<-function(far){
  cel<-(far-32)*5/9
  return(cel)
}

class(ftoc)

#in functions the list of variables to be passed is indicated
#in the function definition (in this case far)
#and a value is returned by the return function at the end of the function (in this case cel)
#the curly brackets delimit the function
#and will delimit the control flow operators we will see below.

#calling the function with an input of 5 (degrees F)
ftoc(5)

#calling the function with an iput of -9999 (degrees F)
ftoc(-9999)

#can't meaningfully go below zero kelvin !
zerokelvin_f<- -459.67

#a more complex function that handles invalid temperatures
#overwriting the previous function!
ftoc<-function(far){
  if(far < zerokelvin_f){
    return(NA)
  }else{
    cel <- (far-32)*5/9
    return(cel)
  }
}

#note that the if/then operator uses round brackets for the boolean argument
#and curly brackets to delimit the code for true and false branches
#the various control flow operators are similarly structured and 
#can be nested inside each other as we'll see below


ftoc(-9999)

#but what if we to round off the results
?round

#creating a new function that rounds by default
#but we can turn off rounding optionally
ftoc<-function(far,rounddown=T){
  if(far < zerokelvin_f){
    return(NA)
  }else{
    cel <- (far-32)*5/9
    if(rounddown==T){
      return(round(cel))
    }else{
      return(cel)
    }
  }
}


#looping over and printing the converted temperatures to the console
#using the for control structure
#"in" returns each element of the list in order over each ieration
#of the loop
for(num in mynums){
  print(ftoc(num))
}

#alernatively we can do it by indexing 
for(i in 1:length(mynums)){
    print(ftoc(mynums[i]))
}

#and there's a much easier way to do it
#by using sapply to operate on the list
?sapply
cvals<-sapply(mynums,ftoc) 
cvals

#can pass arguments into sapplied functions well!
cvals<-sapply(mynums,ftoc,rounddown=T) 
cvals
```
