# 2022 BioCanRx/SCN R Workshop

## Workshop Dates 
14:00-15:30 Eastern Time, September 7, 14, 21, 28 2022 

## Workshop Venue
A Zoom meeting link has been sent to all invited meeting participants.

## Organizers		
* Sarah Ivanco - Manager EDI and Training Programs, BioCanRx
* Dr. Ellie Arnold - Training Coordinator,  Stem Cell Network

## Workshop Files

* [Workshop Agenda](public/2022_BioCanRx-SCN_R_Workshop_Agenda_Posted.pdf?inline=false)

## Additional resources
* [EDX R Programming Courses](https://www.edx.org/learn/r-programming)
* [R Style guide by Hadley Wickham](https://style.tidyverse.org)
* [R language operators](https://www.statmethods.net/management/operators.html)
* [Assignment operators in R](https://www.r-bloggers.com/2010/11/assignment-operators-in-r)
* [An assortment of R cheat sheets](https://www.rstudio.com/resources/cheatsheets/)
* [CRAN R libraries](https://cran.r-project.org/)
* [Bioconductor bioinformatics libraries](https://www.bioconductor.org/)
* [SCN 2022 RNASeq Workshop Content](https://gitlab.com/ohri/2022-scn-rnaseq-workshop)

## Classes 

_Workshop files will be posted here on the day of each class_

### Class 1, September 7: RStudio and the R Language
* [Class 1 Presentation](public/R_Class_1_2022.pptx?inline=FALSE) \[[Video](http://dropbox.ogic.ca/workshop_2022/BioCanRx_SCN_R_course_lecture1.mp4)\]
* [R walkthrough](public/Rwalkthrough_class1.md)


### Class 2, September 14: Data Frames, Libraries, Loading and Saving Data
* [Class 2 Presentation](public/R_Class_2_2022.pptx?inline=FALSE) \[[Video](http://dropbox.ogic.ca/workshop_2022/BioCanRx_SCN_R_course_lecture_2.mp4)\]
* [R walkthrough](public/Rwalkthrough_class2.Rmd)
* [Class 2 Homework](public/class_2_homework.R)

### Class 3, September 21: Accessing Cancer Experimental Data using TCGAbiolinks
* [Class 3 Presentation](public/2022_R_Class_3.pptx?inline=FALSE) \[[Video](http://dropbox.ogic.ca/workshop_2022/BioCanRx_SCN_R_course_lecture_3.mp4)\]
* [R walkthrough](public/Rwalkthrough_class3.Rmd)
* [Class 3 Homework](public/Class3_homework.md)

### Class 4, September 28: Data analysis and Visualization
* [Class 3 Presentation](public/R_Class_4_2022.pptx?inline=FALSE)  \[[Video](http://dropbox.ogic.ca/workshop_2022/BioCanRx_SCN_R_course_lecture_4.mp4)\]
* [R walkthrough](public/Rwalkthrough_class4.Rmd)

